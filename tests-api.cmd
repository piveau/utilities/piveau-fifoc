echo off

IF NOT [%1] == [] set LOCAL_PORT=%1
IF [%1] == [] set LOCAL_PORT=5000
IF NOT [%2] == [] set DOCKER_PORT=%2
IF [%2] == [] set DOCKER_PORT=9090

mkdir out
echo PDF =^> txt
curl -X POST --data-binary @testdata\pdf\Smallpdf.pdf http://localhost:%LOCAL_PORT%/v1/convert/pdf/txt/ > out\Smallpdf.txt

echo JSON =^> xml
curl -X POST --data-binary @testdata\json\example_1.json http://localhost:%LOCAL_PORT%/v1/convert/json/xml/ > out\example_1.xml

echo XLSX =^> csv
curl -X POST --data-binary @testdata\ms\simple.xlsx http://localhost:%LOCAL_PORT%/v1/convert/xlsx/csv/ > out\bus-stops.csv

echo XLS =^> csv
curl -X POST --data-binary @testdata\ms\simple.xls http://localhost:%LOCAL_PORT%/v1/convert/xls/csv/ > out\simple.csv
curl -X POST --data-binary @testdata\ms\tests-example.xls http://localhost:%LOCAL_PORT%/v1/convert/xls/csv/ > out\tests-example.csv


echo CSV =^> docx, html, json, odt, rtf, xls, xlsx, xml
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/json/ > out\username.json
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/docx/ > out\username.docx
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/html/ > out\username.html
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/json/ > out\username.json
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/odt/ > out\username.odt
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/rtf/ > out\username.rtf
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/xls/ > out\username.xls
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/xlsx/ > out\username.xlsx
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%LOCAL_PORT%/v1/convert/csv/xml/ > out\username.xml


echo DOCX =^> pptx, odt, pdf, txt, html, json, odt, rtf
curl -X POST --data-binary @testdata\ms\simple.docx http://localhost:%LOCAL_PORT%/v1/convert/docx/pptx/ > out\simple.pptx
curl -X POST --data-binary @testdata\ms\simple.docx http://localhost:%LOCAL_PORT%/v1/convert/docx/odt/ > out\simple.odt
curl -X POST --data-binary @testdata\ms\simple.docx http://localhost:%LOCAL_PORT%/v1/convert/docx/pdf/ > out\simple.pdf
curl -X POST --data-binary @testdata\ms\simple.docx http://localhost:%LOCAL_PORT%/v1/convert/docx/txt/ > out\simple.txt
curl -X POST --data-binary @testdata\ms\simple.docx http://localhost:%LOCAL_PORT%/v1/convert/docx/html/ > out\simple.html
curl -X POST --data-binary @testdata\ms\simple.docx http://localhost:%LOCAL_PORT%/v1/convert/docx/json/ > out\simple.json
curl -X POST --data-binary @testdata\ms\simple.docx http://localhost:%LOCAL_PORT%/v1/convert/docx/odt/ > out\simple.odt
curl -X POST --data-binary @testdata\ms\simple.docx http://localhost:%LOCAL_PORT%/v1/convert/docx/rtf/ > out\simple.rtf

echo HTML =^> pdf, docx, json, odt, rtf
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%LOCAL_PORT%/v1/convert/html/docx/ > out\simple.docx
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%LOCAL_PORT%/v1/convert/html/json/ > out\simple.json
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%LOCAL_PORT%/v1/convert/html/odt/ > out\simple.odt
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%LOCAL_PORT%/v1/convert/html/rtf/ > out\simple.rtf
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%LOCAL_PORT%/v1/convert/html/pdf/ > out\simple.pdf

echo ODT =^> docx, html, json, rtf
curl -X POST --data-binary @testdata\odt\simple.odt http://localhost:%LOCAL_PORT%/v1/convert/odt/docx/ > out\simple.docx
curl -X POST --data-binary @testdata\odt\simple.odt http://localhost:%LOCAL_PORT%/v1/convert/odt/html/ > out\simple.html
curl -X POST --data-binary @testdata\odt\simple.odt http://localhost:%LOCAL_PORT%/v1/convert/odt/json/ > out\simple.json
curl -X POST --data-binary @testdata\odt\simple.odt http://localhost:%LOCAL_PORT%/v1/convert/odt/rtf/ > out\simple.rtf


echo ######converting using input URL######
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/xls/?url=https://opendata.leipzig.de/dataset/68a48da4-9fa1-4c63-97c7-c597322a81f1/resource/21eb0e67-d53f-49e3-82c4-c492d1d26dd0/download/angebotsmietenlwb.csv > out\angebotsmietenlwb.xls
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/xlsx/?url=https://opendata.schleswig-holstein.de/dataset/d6e23e72-506b-42f8-b14c-13d416b35e08/resource/43c02644-dae3-4013-b236-5b4884f1db7f/download/kreis-segeberg-latin1.csv > out\kreis-segeberg-latin1.xlsx
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/xlsx/?url=https://opendata.leipzig.de/dataset/68a48da4-9fa1-4c63-97c7-c597322a81f1/resource/21eb0e67-d53f-49e3-82c4-c492d1d26dd0/download/angebotsmietenlwb.csv > out\angebotsmietenlwb.xlsx
curl http://localhost:%LOCAL_PORT%/v1/convert/xlsx/csv/?url=http://transparenz.bremen.de/sixcms/media.php/bremen02.a.13.de/download/IST-Daten_2018_Stadtgemeinde_Bremen.xlsx > out\IST-Daten_2018_Stadtgemeinde_Bremen.csv
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/html/?url=https://cohesiondata.ec.europa.eu/resource/99js-gm52.csv > out\99js-gm52.html
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/json/?url=https://cohesiondata.ec.europa.eu/resource/99js-gm52.csv > out\99js-gm52.json
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/json/?url=https://github.com/alimanfoo/csvvalidator/blob/master/example-data-bad.csv > out\example-data-bad.json
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/json/?url=https://transparenz.bremen.de/sixcms/media.php/bremen02.a.13.de/download/Beteiligungsbericht_2018_Mehrjahresvergleich_Beteiligungen.csv > out\Beteiligungsbericht_2018_Mehrjahresvergleich_Beteiligungen.json
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/html/?url=https://transparenz.bremen.de/sixcms/media.php/bremen02.a.13.de/download/Beteiligungsbericht_2018_Mehrjahresvergleich_Beteiligungen.csv > out\Beteiligungsbericht_2018_Mehrjahresvergleich_Beteiligungen.html
curl http://localhost:%LOCAL_PORT%/v1/convert/csv/json/?url=https://transparenz.bremen.de/sixcms/media.php/bremen02.a.13.de/download/Beteiligungsbericht_2018_Mehrjahresvergleich_Beteiligungen.csv > out\Beteiligungsbericht_2018_Mehrjahresvergleich_Beteiligungen.json
curl http://localhost:%LOCAL_PORT%/v1/convert/xlsx/csv/?url=http://transparenz.bremen.de/sixcms/media.php/bremen02.a.13.de/download/IST-Daten_2018_Stadtgemeinde_Bremen.xlsx > out\IST-Daten_2018_Stadtgemeinde_Bremen.csv
curl "http://localhost:%LOCAL_PORT%/v1/convert/csv/json/?url=https://www.statistik.sachsen.de/genonline/online?sequenz=tabelleDownload&selectionname=61261-096&regionalschluessel=" > out\61261-096.json

echo ######converting using Docker######
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%DOCKER_PORT%/v1/convert/html/pdf/ > out\simple.pdf
curl -X POST --data-binary @testdata\json\example_1.json http://localhost:%DOCKER_PORT%/v1/convert/json/xml/ > out\example_1.xml
curl -X POST --data-binary @testdata\json\example_2.json http://localhost:%DOCKER_PORT%/v1/convert/json/xml/ > out\example_2.xml
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%DOCKER_PORT%/v1/convert/csv/xlsx/ > out\username.xlsx
curl -X POST --data-binary @testdata\csv\username.csv http://localhost:%DOCKER_PORT%/v1/convert/csv/json/ > out\username.json
curl -X POST --data-binary @testdata\ms\middle.docx http://localhost:%DOCKER_PORT%/v1/convert/docx/pdf/ > out\middle.pdf
curl -X POST --data-binary @testdata\ms\Input.docx http://localhost:%DOCKER_PORT%/v1/convert/docx/pdf/ > out\Input.pdf
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%DOCKER_PORT%/v1/convert/html/docx/ > out\simple.docx
curl -X POST --data-binary @testdata\odt\simple.odt http://localhost:%DOCKER_PORT%/v1/convert/odt/html/ > out\simple.html
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%DOCKER_PORT%/v1/convert/html/rtf/ > out\simple.rtf
curl -X POST --data-binary @testdata\html\simple.html http://localhost:%DOCKER_PORT%/v1/convert/html/json/ > out\simple.json
curl -X POST --data-binary @testdata\pdf\pdf-sample.pdf http://localhost:%DOCKER_PORT%/v1/convert/pdf/docx/ > out\pdf-sample.docx
curl -X POST --data-binary @testdata\ms\Sample500.xlsx http://localhost:%DOCKER_PORT%/v1/convert/xlsx/html/ > out\Sample500.html
curl http://localhost:%DOCKER_PORT%/v1/convert/csv/xls/?url=https://opendata.leipzig.de/dataset/68a48da4-9fa1-4c63-97c7-c597322a81f1/resource/21eb0e67-d53f-49e3-82c4-c492d1d26dd0/download/angebotsmietenlwb.csv > out\angebotsmietenlwb.xls
curl http://localhost:%DOCKER_PORT%/v1/convert/csv/xlsx/?url=https://opendata.schleswig-holstein.de/dataset/d6e23e72-506b-42f8-b14c-13d416b35e08/resource/43c02644-dae3-4013-b236-5b4884f1db7f/download/kreis-segeberg-latin1.csv > out\kreis-segeberg-latin1.xlsx
curl http://localhost:%DOCKER_PORT%/v1/convert/csv/xlsx/?url=https://opendata.leipzig.de/dataset/68a48da4-9fa1-4c63-97c7-c597322a81f1/resource/21eb0e67-d53f-49e3-82c4-c492d1d26dd0/download/angebotsmietenlwb.csv > out\angebotsmietenlwb.xlsx
curl http://localhost:%DOCKER_PORT%/v1/convert/docx/pdf/?url=https://file-examples.com/wp-content/uploads/2017/02/file-sample_100kB.docx > out\file-sample_100kB.pdf