import os
import pathlib
import tempfile
import unittest

from converters.json_converter import JsonConverter

tempdir = pathlib.Path(tempfile.mkdtemp())


class TestCasesJson2Any(unittest.TestCase):

    def setUp(self):
        here = os.path.dirname(os.path.abspath(__file__))
        testdata_path = os.path.dirname(here)
        self.testdata_path = os.path.join(testdata_path, 'testdata', 'json')
        print(self.testdata_path)

    def test_convert_json_example1_2_xml(self):
        """Test converting a JSON to XML"""
        out_format = 'xml'
        infile = os.path.join(self.testdata_path, 'example_1.json')
        out = tempdir / "{}.{}".format("outfile", out_format)
        json_converter = JsonConverter()
        json_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 137)  # add assertion here

    def test_convert_json_example2_2_xml(self):
        """Test converting a JSON to XML"""
        out_format = 'xml'
        infile = os.path.join(self.testdata_path, 'example_2.json')
        out = tempdir / "{}.{}".format("outfile", out_format)
        json_converter = JsonConverter()
        json_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 1100)  # add assertion here


if __name__ == '__main__':
    unittest.main()
