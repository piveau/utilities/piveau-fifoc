import os
import pathlib
import tempfile
import unittest

from converters.pdf_converter import PdfConverter

in_format = 'pdf'
tempdir = pathlib.Path(tempfile.mkdtemp())


class TestCasesPdf2Any(unittest.TestCase):

    def setUp(self):
        here = os.path.dirname(os.path.abspath(__file__))
        testdata_path = os.path.dirname(here)
        self.testdata_path = os.path.join(testdata_path, 'testdata', 'pdf')

        print(self.testdata_path)

    def test_convert_pdf2txt(self):
        """Test converting PDF to TXT"""
        out_format = 'txt'

        infile = os.path.join(self.testdata_path, 'Smallpdf.pdf')

        out = tempdir / "{}.{}".format("outfile", out_format)
        pdf_converter = PdfConverter()
        pdf_converter.convert(infile, out_format, out)
        self.assertEqual(os.path.getsize(out), 823)  # add assertion here

    def test_convert_example_PDF2txt(self):
        """Test converting example_PDF to TXT"""
        out_format = 'txt'
        infile = os.path.join(self.testdata_path, 'pdf-sample.pdf')
        out = tempdir / "{}.{}".format("outfile", out_format)
        pdf_converter = PdfConverter()
        pdf_converter.convert(infile, out_format, out)
        self.assertEqual(os.path.getsize(out), 1054)  # add assertion here

    # https://www.thepythoncode.com/article/convert-pdf-files-to-docx-in-python

    def test_convert_example_PDFdocx(self):
        """Test converting example_PDF to docx"""
        out_format = 'docx'

        infile = os.path.join(self.testdata_path, 'pdf-sample.pdf')
        out = tempdir / "{}.{}".format("outfile", out_format)
        pdf_converter = PdfConverter()
        pdf_converter.pdf_to_docx(infile, out)

        self.assertEqual(os.path.getsize(out), 37632)  # add assertion here

    def test_convert_small_PDFdocx(self):
        """Test converting small_PDF to docx"""
        out_format = 'docx'

        infile = os.path.join(self.testdata_path, 'Smallpdf.pdf')
        out = tempdir / "{}.{}".format("outfile", out_format)
        print(out)
        pdf_converter = PdfConverter()
        pdf_converter.pdf_to_docx(infile, out)

        self.assertEqual(os.path.getsize(out), 140331)  # add assertion here


if __name__ == '__main__':
    unittest.main()
