import os
import pathlib
import tempfile
import unittest

from converters.docx_converter import DocxConverter
from converters.excel_converter import ExcelConverter

tempdir = pathlib.Path(tempfile.mkdtemp())


class TestCasesOffice2Any(unittest.TestCase):

    def setUp(self):
        here = os.path.dirname(os.path.abspath(__file__))
        testdata_path = os.path.dirname(here)
        self.testdata_path = os.path.join(testdata_path, 'testdata', 'ms')
        print(self.testdata_path)

    def test_convert_docx2pdf(self):
        """Test converting a DOCX to PDF"""
        out_format = 'pdf'
        infile = os.path.join(self.testdata_path, 'simple.docx')
        out = tempdir / "{}.{}".format("outfile", out_format)
        docx_converter = DocxConverter()
        docx_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 1548303)  # add assertion here

    # DEPRECATED
    # def test_convert_docx2rtf(self):
    #     """Test converting a DOCX to RTF"""
    #     out_format = 'rtf'
    #     infile = os.path.join(self.testdata_path, 'simple.docx')
    #     out = tempdir / "{}.{}".format("outfile", out_format)
    #     docx_converter = DocxConverter()
    #     docx_converter.convert(infile, out_format, out)
    #     print(out)
    #     print(os.path.getsize(out))
    #     self.assertEqual(os.path.getsize(out), 501793113)  # add assertion here

    def test_convert_docx2txt(self):
        """Test converting a DOCX to TXT"""
        out_format = 'txt'
        infile = os.path.join(self.testdata_path, 'middle.docx')
        out = tempdir / "{}.{}".format("outfile", out_format)
        docx_converter = DocxConverter()
        docx_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 32152)  # add assertion here

    def test_convert_excel2csv(self):
        """Test converting a EXCEL to CSV"""
        out_format = 'csv'
        infile = os.path.join(self.testdata_path, 'simple.xlsx')
        out = tempdir / "{}.{}".format("outfile", out_format)
        excel_converter = ExcelConverter()
        excel_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 157)  # add assertion here

    def test_convert_excel2csv_10k(self):
        """Test converting a EXCEL to CSV"""
        out_format = 'csv'
        infile = os.path.join(self.testdata_path, 'Sample10K.xlsx')
        out = tempdir / "{}.{}".format("outfile", out_format)
        excel_converter = ExcelConverter()
        excel_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 687916)  # add assertion here

    def test_convert_excel_sales2csv_10k(self):
        """Test converting a EXCEL SALES to CSV"""
        out_format = 'csv'
        infile = os.path.join(self.testdata_path, 'Sample-sales-data-excel.xlsx')
        out = tempdir / "{}.{}".format("outfile", out_format)
        excel_converter = ExcelConverter()
        excel_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 2437184)  # add assertion here

    def test_convert_xls2csv(self):
        """Test converting a XLS to CSV"""
        out_format = 'csv'
        infile = os.path.join(self.testdata_path, 'simple.xls')
        out = tempdir / "{}.{}".format("outfile", out_format)
        excel_converter = ExcelConverter()
        excel_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 157)  # add assertion here

    def test_convert_xls_example2csv(self):
        """Test converting a XLS example to CSV"""
        out_format = 'csv'
        infile = os.path.join(self.testdata_path, 'tests-example.xls')
        out = tempdir / "{}.{}".format("outfile", out_format)
        excel_converter = ExcelConverter()
        excel_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 579)  # add assertion here

    def test_convert_xls_example102csv(self):
        """Test converting a XLS example 10 to CSV"""
        out_format = 'csv'
        infile = os.path.join(self.testdata_path, 'file_example_XLS_10.xls')
        out = tempdir / "{}.{}".format("outfile", out_format)
        excel_converter = ExcelConverter()
        excel_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 572)  # add assertion here

    def test_convert_excel2html_500(self):
        """Test converting a XLS example 500 to HTML"""
        out_format = 'html'
        infile = os.path.join(self.testdata_path, 'Sample500.xlsx')
        out = tempdir / "{}.{}".format("outfile", out_format)
        excel_converter = ExcelConverter()
        excel_converter.convert(infile, out_format, out)
        print(out)
        print(os.path.getsize(out))
        self.assertEqual(os.path.getsize(out), 1072478)  # add assertion here

if __name__ == '__main__':
    unittest.main()
