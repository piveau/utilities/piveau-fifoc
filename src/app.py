import logging
import os
from flask import Flask
from util.app_configuration import AppConfiguration


def create_app() -> Flask:
    app = Flask(__name__)
    app.logger.setLevel(logging.DEBUG)
    app.url_map.strict_slashes = False
    # Read system configuration
    conf = AppConfiguration()
    conf.read_config()
    logging.basicConfig(filename=conf.get_logfile(), level=conf.get_logging_level())

    with app.app_context():
        import routes

    return app


if __name__ == "__main__":
    app = create_app()
    port = int(os.environ.get('PORT', 5000))
    app.run(port=port, debug=True)
