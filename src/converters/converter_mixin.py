from util.app_configuration import AppConfiguration
from flask import current_app


class ConverterMixin:
    def __init__(self):
        self._log = current_app.logger
        self._config = AppConfiguration()
