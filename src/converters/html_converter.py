import pdfkit
from converters.converter_mixin import ConverterMixin


__ACCEPTED_HTML_FORMATS_STR = """html"""

ACCEPTED_HTML_INPUT_FORMATS = {
    in_format.strip()
    for in_format in __ACCEPTED_HTML_FORMATS_STR.strip().split(",")
}

__ACCEPTED_HTML_OUTPUT_FORMATS_STR = """
pdf
"""
ACCEPTED_HTML_OUTPUT_FORMATS = {
    out_format.strip()
    for out_format in __ACCEPTED_HTML_OUTPUT_FORMATS_STR.strip().split(",")
}


class HtmlConverter(ConverterMixin):

    def convert(self, infile, out_format, outfile):
        self._log.debug("convert try to convert file: %s " % str(infile))

        if out_format == "pdf":
            self.html_to_pdf(infile, outfile)
        else:
            return "Invalid output format for HTML", 404

        self._log.debug("convert done well.")

    def html_to_pdf(self, html_file, pdf_file):
        self._log.debug("html_to_pdf try to convert file: %s " % str(html_file))
        pdfkit.from_file(str(html_file), str(pdf_file))
        self._log.debug("html_to_pdf done well.")
