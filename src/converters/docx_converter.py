import pypandoc
import docx2txt

from converters.converter_mixin import ConverterMixin

__ACCEPTED_DOCX_FORMATS_STR = """docx"""

ACCEPTED_DOCX_INPUT_FORMATS = {
    in_format.strip()
    for in_format in __ACCEPTED_DOCX_FORMATS_STR.strip().split(",")
}

__ACCEPTED_DOCX_OUTPUT_FORMATS_STR = """
pdf, txt
"""

ACCEPTED_DOCX_OUTPUT_FORMATS = {
    out_format.strip()
    for out_format in __ACCEPTED_DOCX_OUTPUT_FORMATS_STR.strip().split(",")
}


class DocxConverter(ConverterMixin):

    def __init__(self):
        super().__init__()

    def convert(self, infile, out_format, outfile):
        self._log.debug("convert try to convert file: %s " % str(infile))
        if out_format == "pdf":
            self.docx_to_pdf(infile, outfile)
        elif out_format == "txt":
            self.docx_to_txt(infile, outfile)
        else:
            return "Invalid output format for DOCX", 404

        self._log.info("convert done well.")

    def docx_to_pdf(self, docx_file, pdf_file):

        self._log.debug("docx_to_pdf try to convert file: %s " % str(docx_file))
        pypandoc.convert_file(str(docx_file), "latex", outputfile=str(pdf_file))
        self._log.debug("docx_to_pdf file: %s done." % str(pdf_file))

    def docx_to_txt(self, docx_file, txt_file):

        self._log.debug("docx_to_txt try to convert file: %s " % str(docx_file))
        text = docx2txt.process(docx_file)
        with open(txt_file, 'w', encoding='utf-8') as text_file_stream:
            text_file_stream.write(text)
        self._log.debug("docx_to_txt file: %s done." % str(txt_file))
