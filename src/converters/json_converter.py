from json2xml import json2xml
from json2xml.utils import readfromjson
from converters.converter_mixin import ConverterMixin


__ACCEPTED_JSON_FORMATS_STR = """json"""

ACCEPTED_JSON_INPUT_FORMATS = {
    in_format.strip()
    for in_format in __ACCEPTED_JSON_FORMATS_STR.strip().split(",")
}

__ACCEPTED_JSON_OUTPUT_FORMATS_STR = """
xml
"""
ACCEPTED_JSON_OUTPUT_FORMATS = {
    out_format.strip()
    for out_format in __ACCEPTED_JSON_OUTPUT_FORMATS_STR.strip().split(",")
}


class JsonConverter(ConverterMixin):

    def convert(self, infile, out_format, outfile):
        self._log.debug("convert try to convert file: %s " % str(infile))

        if out_format == "xml":
            self.json_to_xml(infile, outfile)
        else:
            return "Invalid output format for JSON", 404

        self._log.debug("convert done well.")

    def json_to_xml(self, json_file, xml_file):
        self._log.debug("json_to_xml try to convert file: %s " % str(json_file))
        json_data = readfromjson(json_file)
        xml_data = json2xml.Json2xml(json_data).to_xml()
        with open(xml_file, 'w', encoding='utf-8') as xml_f:
            xml_f.write(xml_data)  # type: ignore
        self._log.debug("json_to_xml done well.")
