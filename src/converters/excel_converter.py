import io
import pathlib

import pandas as pd
from converters.converter_mixin import ConverterMixin

from xlsx2html import xlsx2html

__ACCEPTED_XLSX_FORMATS_STR = """xlsx, xls"""

ACCEPTED_XLSX_INPUT_FORMATS = {
    in_format.strip()
    for in_format in __ACCEPTED_XLSX_FORMATS_STR.strip().split(",")
}

__ACCEPTED_XLSX_OUTPUT_FORMATS_STR = """
csv, html
"""

ACCEPTED_XLSX_OUTPUT_FORMATS = {
    out_format.strip()
    for out_format in __ACCEPTED_XLSX_OUTPUT_FORMATS_STR.strip().split(",")
}


class ExcelConverter(ConverterMixin):

    def convert(self, infile, out_format, outfile):
        self._log.debug("convert try to convert file: %s " % str(infile))
        if out_format == "csv":
            self.excel_to_csv(infile, outfile)
        elif out_format == "html" and pathlib.Path(infile).suffix == ".xlsx":
            self.xlsx_to_html(infile, outfile)

        else:
            return "Invalid output format for EXCEL", 404

        self._log.info("convert done well.")

    def excel_to_csv(self, excel_file, csv_file):

        self._log.debug("excel_to_csv try to convert file: %s " % str(csv_file))

        read_file = pd.read_excel(excel_file)
        read_file.to_csv(csv_file, index=True, header=True)
        self._log.debug("excel_to_pdf file: %s done." % str(csv_file))

    def xlsx_to_html(self, xlsx_file, html_file):

        self._log.debug("xlsx_to_html try to convert file: %s " % str(html_file))
        print(html_file)
        print(xlsx_file)
        xlsx_file = open(xlsx_file, 'rb')
        html_file_str_io = io.StringIO()
        xlsx2html(xlsx_file, html_file_str_io, locale='en')
        html_file = open(html_file, "w")
        html_file_str_io.seek(0)
        html_file.write(html_file_str_io.read())
        html_file.close()
        self._log.debug("xlsx_to_html file: %s done." % str(html_file))
