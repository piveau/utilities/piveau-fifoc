import csv
import json
import openpyxl
import pandas as pd
import chardet
import codecs
import xml.etree.ElementTree as ET


from converters.converter_mixin import ConverterMixin


__ACCEPTED_CSV_FORMATS_STR = """csv"""

ACCEPTED_CSV_INPUT_FORMATS = {
    in_format.strip()
    for in_format in __ACCEPTED_CSV_FORMATS_STR.strip().split(",")
}

__ACCEPTED_CSV_OUTPUT_FORMATS_STR = """
xlsx, json, xml
"""
ACCEPTED_CSV_OUTPUT_FORMATS = {
    out_format.strip()
    for out_format in __ACCEPTED_CSV_OUTPUT_FORMATS_STR.strip().split(",")
}


# https://stackoverflow.com/questions/69817054/python-detection-of-delimiter-separator-in-a-csv-file
def find_delimiter(filename):
    delimiter = ";"
    sniffer = csv.Sniffer()
    with open(filename) as fp:
        try:
            delimiter = sniffer.sniff(fp.read(5000)).delimiter
        except Exception as e:
            print("find_delimiter:" + str(e))
    return delimiter


class CsvConverter(ConverterMixin):

    def convert(self, infile, out_format, outfile):
        self._log.debug("convert: try to convert file: %s " % str(infile))

        if out_format == "json":
            self.csv_to_json(infile, outfile)
        elif out_format == "xml":
            self.csv_to_xml(infile, outfile)
        elif out_format == "xlsx":
            self.csv_to_excel(infile, outfile)
        else:
            return "Invalid output format for CSV", 404

        self._log.debug("convert done well.")

    def csv_to_excel(self, csv_file, excel_file):
        self._log.debug("csv_to_excel: try to convert file: %s " % str(csv_file))
        csv_data = []
        with open(csv_file, encoding='unicode_escape') as file_obj:
            reader = csv.reader(file_obj)
            for row in reader:
                csv_data.append(row)

        workbook = openpyxl.Workbook()
        sheet = workbook.active
        for row in csv_data:
            sheet.append(row)
        workbook.save(excel_file)
        self._log.debug("csv_to_excel done well with openpyxl.")

    def csv_to_json(self, csv_file, json_file):
        # SOURCE FROM https://stackoverflow.com/q/66543315
        self._log.debug("csv_to_json try to convert file: %s " % str(csv_file))
        with open(csv_file, 'rb') as f:
            content_bytes = f.read()
        detected = chardet.detect(content_bytes)
        encoding = detected['encoding']
        self._log.debug("csv_to_json file is encoded with:" + encoding)
        json_array = []
        with open(csv_file, encoding=encoding) as csv_file_stream:
            # load csv file data using csv library's dictionary reader
            csv_reader = csv.DictReader(csv_file_stream)
            # convert each csv row into python dict
            for row in csv_reader:
                # add this python dict to json array
                json_array.append(row)
        # convert python jsonArray to JSON String and write to file
        with open(json_file, 'w', encoding='utf8') as jsonf:
            json.dump(json_array, jsonf, indent=4, ensure_ascii=False)
        self._log.debug("csv_to_json done well.")

    def csv_to_xls(self, csv_file, xls_file):
        self._log.debug("csv_to_xls try to convert file: %s " % str(csv_file))
        delimiter = find_delimiter(csv_file)

        if str(self._config.get_skip_bad_lines_csv()) == "TRUE":
            self._log.debug("read_csv with on_bad_lines='skip'")
            read_file = pd.read_csv(csv_file, delimiter=delimiter, low_memory=False, on_bad_lines='skip')
        else:
            self._log.debug("read_csv without on_bad_lines='skip'")
            read_file = pd.read_csv(csv_file, delimiter=delimiter, low_memory=False)

        result_excel_file = pd.ExcelWriter(xls_file)
        read_file.to_excel(result_excel_file, index=False, header=True)
        result_excel_file.close()

        self._log.debug("csv_to_xls done well.")

    def convert_codec2utf8(self, infile):
        self._log.debug("converting to uft-8")
        with open(infile, 'rb') as f:
            content_bytes = f.read()
        detected = chardet.detect(content_bytes)
        encoding = detected['encoding']
        self._log.debug(f"{infile}: detected as {encoding}.")
        if encoding != "utf-8":
            content_text = content_bytes.decode(encoding)
            file = codecs.open(infile, "w", "utf-8")
            file.write(content_text)
            file.close()
        else:
            self._log.debug("NO need to convert to uft-8")

    # source https://blog.finxter.com/5-best-ways-to-convert-csv-to-xml-in-python/
    def csv_to_xml(self, csv_file, xml_file):
        self._log.debug("csv_to_xml try to convert file: %s " % str(xml_file))
        # Read the CSV and add data to an XML file
        with open(csv_file, 'r') as csvfile:
            csvreader = csv.reader(csvfile)
            headers = next(csvreader)

            root = ET.Element('Data')
            for row in csvreader:
                child = ET.Element('Record')
                for i, elem in enumerate(headers):
                    child_element = ET.Element(elem)
                    child_element.text = row[i]
                    child.append(child_element)
                root.append(child)

            tree = ET.ElementTree(root)
            ET.indent(tree, space="\t", level=0)
            tree.write(xml_file, encoding="utf-8")
        self._log.debug("csv_to_xml done well.")
