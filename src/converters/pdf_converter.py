from PyPDF2 import PdfReader
# comment this out for run local
from pdf2docx import Converter

from converters.converter_mixin import ConverterMixin


__ACCEPTED_PDF_FORMATS_STR = """pdf"""

ACCEPTED_PDF_INPUT_FORMATS = {
    in_format.strip()
    for in_format in __ACCEPTED_PDF_FORMATS_STR.strip().split(",")
}

__ACCEPTED_PDF_OUTPUT_FORMATS_STR = """
txt, docx
"""
ACCEPTED_PDF_OUTPUT_FORMATS = {
    out_format.strip()
    for out_format in __ACCEPTED_PDF_OUTPUT_FORMATS_STR.strip().split(",")
}


class PdfConverter(ConverterMixin):

    def convert(self, infile, out_format, outfile):
        self._log.debug("convert try to convert file: %s " % str(infile))
        if out_format == "txt":
            self.pdf_to_txt(infile, outfile)
        elif out_format == "docx":
            self.pdf_to_docx(infile, outfile)
        else:
            return "Invalid output format for PDF", 404

        self._log.debug("convert done well.")

    def pdf_to_txt(self, pdf_file, txt_file):
        self._log.debug("pdf_to_txt try to convert file: %s " % str(pdf_file))
        # SOURCE FROM https://stackoverflow.com/a/63518022
        reader = PdfReader(pdf_file)
        number_of_pages = len(reader.pages)
        output = ''
        for i in range(number_of_pages):
            page = reader.pages[i]
            output += page.extract_text()
        with open(txt_file, 'w', encoding='utf-8') as text_f:
            text_f.write(output)
        self._log.debug("pdf_to_txt done well.")

    def pdf_to_docx(self, pdf_file, docx_file):
        self._log.debug("pdf_to_docx try to convert file: %s " % str(pdf_file))
        cv = Converter(pdf_file)
        cv.convert(docx_file)
        self._log.debug("pdf_to_docx done well.")
