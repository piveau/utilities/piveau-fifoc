import os
import pathlib
import tempfile
from logging import Logger
from pathlib import Path

import requests
from flask.wrappers import Request

from converters.csv_converter import (ACCEPTED_CSV_INPUT_FORMATS,
                                      ACCEPTED_CSV_OUTPUT_FORMATS,
                                      CsvConverter)
from converters.docx_converter import (ACCEPTED_DOCX_INPUT_FORMATS,
                                       ACCEPTED_DOCX_OUTPUT_FORMATS,
                                       DocxConverter)
from converters.excel_converter import (ACCEPTED_XLSX_INPUT_FORMATS,
                                        ACCEPTED_XLSX_OUTPUT_FORMATS,
                                        ExcelConverter)
from converters.html_converter import (ACCEPTED_HTML_INPUT_FORMATS,
                                       ACCEPTED_HTML_OUTPUT_FORMATS,
                                       HtmlConverter)
from converters.json_converter import (ACCEPTED_JSON_INPUT_FORMATS,
                                       ACCEPTED_JSON_OUTPUT_FORMATS,
                                       JsonConverter)
from converters.pandoc_converter import (ACCEPTED_PANDOC_INPUT_FORMATS,
                                         ACCEPTED_PANDOC_OUTPUT_FORMATS,
                                         PandocConverter)
from converters.pdf_converter import (ACCEPTED_PDF_INPUT_FORMATS,
                                      ACCEPTED_PDF_OUTPUT_FORMATS,
                                      PdfConverter)
from util.app_configuration import AppConfiguration



CHROME_______SAFARI___ = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36'

REQUEST_HEADERS = {
    "Connection": "Keep-Alive",
    'User-Agent': CHROME_______SAFARI___}


IN_EXTENSIONS = {
    "yaml": "yaml",
    "xlsx": "xlsx",
    "json": "json",
    "csv": "csv",
    "docx": "docx",
    "latex": "tex",
    "markdown": "md",
    "markdown_github": "md",
    "markdown_mmd": "md",
    "markdown_phpextra": "md",
    "markdown_strict": "md",
}

OUTPUT_FILE_EXTENSION_MAPPING = {
    "yaml": "yaml",
    "xlsx": "xlsx",
    "json": "json",
    "csv": "csv",
    "docx": "docx",
    "pdf": "pdf",
    "beamer": "pdf",
    "latex": "pdf",
    "html5": "html",
    "revealjs": "html",
    "markdown": "md",
    "markdown_github": "md",
    "markdown_mmd": "md",
    "markdown_phpextra": "md",
    "markdown_strict": "md",
    "rdf": "nt",
}


def download_url2file_chunk(url: str, file: Path, headers) -> None:
    """Download the file from the url and save in the file with the given path"""
    # NOTE the stream=True parameter below
    # SOURCE from https://stackoverflow.com/a/16696317
    conf = AppConfiguration()
    conf.read_config()
    with requests.get(url, headers=headers, stream=True, allow_redirects=True) as r:
        content_length = r.headers.get("Content-length")
        if content_length:
            print('download_url2file_chunk->Content-length:' + content_length)
            if int(conf.get_max_file_size()) < int(content_length):
                raise FIFOCException(
                    'Content Too Large: ' + sizeof_fmt(int(content_length)) + ', Max Content size to convert is ' +
                    sizeof_fmt(int(conf.get_max_file_size())), 413)
        r.raise_for_status()
        with open(file, 'wb') as f:
            for chunk in r.iter_content(chunk_size=81920):
                # If you have chunk encoded response uncomment if
                # and set chunk_size parameter to None.
                # if chunk:
                f.write(chunk)
    print("\ndownload_url2file_chunk done well.")


def exists(path):
    r = requests.head(path)
    return r.status_code == requests.codes.ok


def download_url2file(url: str, file: Path) -> None:
    """Download the file from the url and save in the file with the given path"""
    # no chunking
    conf = AppConfiguration()
    conf.read_config()

    with requests.get(url, headers=REQUEST_HEADERS, stream=True, verify=False) as r:
        # print("download_url2file r.status_code=" + str(r.status_code))
        if r.status_code == 200:
            print('download_url2file r.status_code=OK!')
        else:
            raise FIFOCException('Source file not available', r.status_code)

        content_length = r.headers.get("Content-length")
        if content_length:
            #print('download_url2file->Content-length=' + content_length)
            if int(conf.get_max_file_size()) < int(content_length):
                raise FIFOCException(
                    'Content Too Large: ' + sizeof_fmt(int(content_length)) + ', Max Content size to convert is ' +
                    sizeof_fmt(int(conf.get_max_file_size())), 413)
        r.raise_for_status()
        open(file, 'wb').write(r.content)
        print("\ndownload_url2file done well.")


def upload_file_as_stream(req: Request, file: Path) -> None:
    """Uploads the received binary file in POST payload to the file in the given path"""
    with open(file, "wb") as f:
        chunk_size = 81920
        while True:
            chunk = req.stream.read(chunk_size)
            if len(chunk) == 0:
                return
            f.write(chunk)



def general_convert(infile: Path, in_format: str,
                    out_format: str, outfile: Path) -> None:
    """Finds the appropriate converter and does the conversion."""
    file_size = os.path.getsize(infile)
    print("File to convert size is ", sizeof_fmt(file_size))

    conf = AppConfiguration()
    conf.read_config()

    if int(conf.get_max_file_size()) < file_size:
        raise FIFOCException('File Too Large: ' + sizeof_fmt(file_size) + ', Max File size to convert is ' +
                             sizeof_fmt(int(conf.get_max_file_size())), 413)

    if out_format in ACCEPTED_DOCX_OUTPUT_FORMATS and in_format in ACCEPTED_DOCX_INPUT_FORMATS:
        docx_converter = DocxConverter()
        docx_converter.convert(infile, out_format, outfile)
    elif out_format in ACCEPTED_HTML_OUTPUT_FORMATS and in_format in ACCEPTED_HTML_INPUT_FORMATS:
        html_converter = HtmlConverter()
        html_converter.convert(infile, out_format, outfile)
    elif out_format in ACCEPTED_XLSX_OUTPUT_FORMATS and in_format in ACCEPTED_XLSX_INPUT_FORMATS:
        excel_converter = ExcelConverter()
        excel_converter.convert(infile, out_format, outfile)
    elif out_format in ACCEPTED_CSV_OUTPUT_FORMATS and in_format in ACCEPTED_CSV_INPUT_FORMATS:
        csv_converter = CsvConverter()
        csv_converter.convert(infile, out_format, outfile)
    elif out_format in ACCEPTED_JSON_OUTPUT_FORMATS and in_format in ACCEPTED_JSON_INPUT_FORMATS:
        json_converter = JsonConverter()
        json_converter.convert(infile, out_format, outfile)
    elif out_format in ACCEPTED_PDF_OUTPUT_FORMATS and in_format in ACCEPTED_PDF_INPUT_FORMATS:
        pdf_converter = PdfConverter()
        pdf_converter.convert(infile, out_format, outfile)
    elif out_format in ACCEPTED_PANDOC_OUTPUT_FORMATS and \
            in_format in ACCEPTED_PANDOC_INPUT_FORMATS:
        pandoc_converter = PandocConverter()
        pandoc_converter.convert(infile, in_format, out_format, outfile)
    else:
        raise FIFOCException("Invalid input/output format", 404)


"""
FROM https://stackoverflow.com/questions/1094841/get-human-readable-version-of-file-size
"""


def sizeof_fmt(num, suffix="B"):
    for unit in ("", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi"):
        if abs(num) < 1024.0:
            return f"{num:3.1f}{unit}{suffix}"
        num /= 1024.0
    return f"{num:.1f}Yi{suffix}"


def get_file_extensions(in_format: str, out_format: str) -> tuple[str, str]:
    """Get the file extensions for input and output files based on their format"""
    out_extension = OUTPUT_FILE_EXTENSION_MAPPING.get(out_format, out_format)
    in_extension = IN_EXTENSIONS.get(in_format, in_format)
    return in_extension, out_extension


def conversion_handler(infile: Path, outfile: Path,
                       in_format: str, out_format: str,
                       request: Request, logger: Logger) -> Path:
    """
    The file conversion logic. The given file, either in binary or url format,
    is saved to a local temp file, and then the conversion is done based on the
    given input and output format.
    """
    # SOURCE BASED ON https://github.com/jbzdak/pandoc-rest-api/blob/master/pandoc_api.py
    if request.method == 'GET':
        url = request.args.get('url')
        if url is None:
            raise FIFOCException("URL cannot be None", 400)

        logger.debug(f'Call GET {in_format}=>{out_format} from URL {url}')
        response = requests.head(url, allow_redirects=True, verify=False)  # type: ignore
        if response.status_code == 200:
            download_url2file(url, infile)
        elif response.status_code == 405:
            logger.debug("HEAD Method not allowed")
            download_url2file(url, infile)
        elif response.status_code == 301:
            logger.debug("Redirect.....")
            download_url2file(url, infile)
        elif response.status_code == 302 or response.status_code == 307:
            logger.debug("Redirect temporarily moved .....")
            download_url2file(url, infile)
        elif response.status_code == 400:
            logger.debug("Bad Request.....")
            download_url2file(url, infile)
        elif response.status_code == 403:
            logger.debug("Got 403 try to get the url with user-agent.....")
            download_url2file(url, infile)
        elif response.status_code == 500:
            logger.debug("Got 500 try to get the url with user-agent.....")
            download_url2file(url, infile)
        else:
            raise FIFOCException(f'Invalid URL: {url}', response.status_code)
    else:  # POST
        logger.debug(f'Call POST {in_format}=>{out_format}')
        upload_file_as_stream(request, infile)

    general_convert(infile, in_format, out_format, outfile)
    logger.debug("Convert done well.")
    return outfile


def get_temp_files(in_extension: str, out_extension: str,
                   infile_name: str = "infile",
                   outfile_name: str = "outfile") -> tuple[Path, Path, Path]:
    """Creates two temp_files for input and output, and returns their path."""
    tempdir = pathlib.Path(tempfile.mkdtemp())
    infile = tempdir / f"{infile_name}.{in_extension}"
    outfile = tempdir / f"{outfile_name}.{out_extension}"
    return infile, outfile, tempdir


class FIFOCException(Exception):
    ...
