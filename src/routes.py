# coding=utf8
import shutil
from typing import Union

import requests
from flask import Response, after_this_request, render_template, request, current_app
from flask.helpers import send_file
import requests
from werkzeug import exceptions
import pandas

import functions

from converters.csv_converter import (ACCEPTED_CSV_INPUT_FORMATS,
                                      ACCEPTED_CSV_OUTPUT_FORMATS)
from converters.docx_converter import (ACCEPTED_DOCX_INPUT_FORMATS,
                                       ACCEPTED_DOCX_OUTPUT_FORMATS)
from converters.excel_converter import (ACCEPTED_XLSX_INPUT_FORMATS,
                                        ACCEPTED_XLSX_OUTPUT_FORMATS)
from converters.html_converter import (ACCEPTED_HTML_INPUT_FORMATS,
                                       ACCEPTED_HTML_OUTPUT_FORMATS)
from converters.json_converter import (ACCEPTED_JSON_INPUT_FORMATS,
                                       ACCEPTED_JSON_OUTPUT_FORMATS)
from converters.pandoc_converter import (ACCEPTED_PANDOC_INPUT_FORMATS,
                                         ACCEPTED_PANDOC_OUTPUT_FORMATS)
from converters.pdf_converter import (ACCEPTED_PDF_INPUT_FORMATS,
                                      ACCEPTED_PDF_OUTPUT_FORMATS)
from functions import FIFOCException

FILE_IS_NOT_ACCESSIBLE_ = "Source file is not accessible!"


@current_app.route('/help')
def get_help():
    current_app.logger.debug("Call get_help..")
    return render_template('help.html')


@current_app.route("/api/hub/fifoc/help", methods=['GET'])
def get_help_context():
    current_app.logger.debug("Call get_help..")
    return render_template('help.html')


@current_app.route('/')
def get_swaggerui():
    current_app.logger.debug("Call swaggerui..")
    return render_template('swaggerui.html')


@current_app.route("/api/hub/fifoc", methods=['GET'])
def get_swaggerui_context():
    current_app.logger.debug("Call swaggerui with context..")
    return render_template('swaggerui.html')


@current_app.route("/v1/convert/<string:in_format>/<string:out_format>/", methods=['POST', 'GET'])
def convert(in_format: str, out_format: str) -> tuple[Union[Response, str], int]:
    current_app.logger.debug('>>>>>>>>>>>>>>>>>>>>>> Call convert %s=>%s', in_format, out_format)
    in_extension, out_extension = functions.get_file_extensions(in_format, out_format)
    current_app.logger.debug(f"in_extension {in_extension}, out_extension {out_extension}")
    infile, outfile, tempdir = functions.get_temp_files(in_extension, out_extension)

    @after_this_request
    def remove_files(response):
        current_app.logger.debug("Clean up...")
        try:
            shutil.rmtree(tempdir)
        except Exception as error:
            current_app.logger.warn(f"Error removing or closing temp file handle {error}")
        return response

    outfile = functions.conversion_handler(infile, outfile, in_format, out_format,
                                           request, current_app.logger)
    return send_file(outfile, as_attachment=True), 200


@current_app.route("/v1/formats/<string:in_format>/", methods=['GET'])
def get_formats(in_format: str) -> tuple[str, int]:
    if in_format in ACCEPTED_DOCX_INPUT_FORMATS:
        return f'Accepted Output Formats: {ACCEPTED_DOCX_OUTPUT_FORMATS}', 200
    elif in_format in ACCEPTED_HTML_INPUT_FORMATS:
        return f'Accepted Output Formats: {ACCEPTED_HTML_OUTPUT_FORMATS}', 200
    elif in_format in ACCEPTED_XLSX_INPUT_FORMATS:
        return f'Accepted Output Formats: {ACCEPTED_XLSX_OUTPUT_FORMATS}', 200
    elif in_format in ACCEPTED_CSV_INPUT_FORMATS:
        return f'Accepted Output Formats: {ACCEPTED_CSV_OUTPUT_FORMATS}', 200
    elif in_format in ACCEPTED_JSON_INPUT_FORMATS:
        return f'Accepted Output Formats: {ACCEPTED_JSON_OUTPUT_FORMATS}', 200
    elif in_format in ACCEPTED_PDF_INPUT_FORMATS:
        return f'Accepted Output Formats: {ACCEPTED_PDF_OUTPUT_FORMATS}', 200
    elif in_format in ACCEPTED_PANDOC_INPUT_FORMATS:
        return f'Accepted Output Formats: {ACCEPTED_PANDOC_OUTPUT_FORMATS}', 200
    else:
        raise FIFOCException("Invalid input format", 404)


@current_app.errorhandler(Exception)
def handle_bad_request(e):
    current_app.logger.debug(e)
    current_app.logger.debug(type(e))

    if 'Pandoc' in str(e) or isinstance(e, pandas.errors.ParserError):
        if "Error tokenizing data" in str(e):
            return "File conversion failed because: " + str(e), 400
        elif "Pandoc died with exitcode" in str(e):
            return "Internal server error, please try again", 500
        elif "No pandoc was found" in str(e):
            current_app.logger.debug("No pandoc was found")
            return "Internal server error, please try again", 500
        
        return 'File conversion failed!', 400
    elif isinstance(e, FIFOCException):
        return e.args
    elif isinstance(e, RuntimeError):
        return 'Internal Server Error!', 500
    elif isinstance(e, requests.exceptions.ConnectionError):
        current_app.logger.debug("requests.exceptions.ConnectionError")
        return "Connection Error", 400
    elif isinstance(e, requests.exceptions.InvalidURL):
        current_app.logger.debug("requests.exceptions.InvalidURL")
        return FILE_IS_NOT_ACCESSIBLE_, 403
    elif isinstance(e, requests.exceptions.ConnectTimeout):
        current_app.logger.debug("requests.exceptions.ConnectTimeout")
        return FILE_IS_NOT_ACCESSIBLE_, 599
    elif isinstance(e, requests.exceptions.RequestException):
        current_app.logger.debug("requests.exceptions.RequestException")
        return FILE_IS_NOT_ACCESSIBLE_, 403
    elif isinstance(e, exceptions.BadRequest):
        current_app.logger.debug("exceptions.BadRequest")
        return FILE_IS_NOT_ACCESSIBLE_, 403
    elif isinstance(e, exceptions.NotFound):
        current_app.logger.debug("exceptions.NotFound")
        if hasattr(e, 'message'):
            return e.message, 403
        return FILE_IS_NOT_ACCESSIBLE_, 403
    elif isinstance(e, requests.exceptions.ConnectionError):
        return "URL is not available", 404
    else:
        return "Internal Server Error!", 500
