FROM python:3.11.8

RUN apt-get update -qy && \
    # apt-get upgrade -qy && \
    #pypandoc needs pandoc. \
    apt-get install pandoc -qy && \
    # too big -> apt-get install texlive-full  -qy && \
    # need texlive for docx->pdf pdflatex
    apt-get install texlive -qy && \
    wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/libssl1.1_1.1.0g-2ubuntu4_amd64.deb&&\
    apt install -qy ./libssl1.1_1.1.0g-2ubuntu4_amd64.deb --allow-downgrades && \
    rm libssl1.1_1.1.0g-2ubuntu4_amd64.deb && \
    # Install wkhtmltopdf & wkhtmltoimage on Debian 10/9 Linux \
    wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb&& \
    apt install -qy ./wkhtmltox_0.12.6-1.buster_amd64.deb && \
    rm wkhtmltox_0.12.6-1.buster_amd64.deb && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/cache/* && \
    rm -rf /var/tmp/* && \
    rm -rf /tmp/* && \
    rm -rf /root/.npm && \
    rm -rf /root/.cache && \
    rm -rf /var/lib/apt/lists/*


RUN groupadd -r uwsgi && useradd -r -g uwsgi uwsgi

COPY requirements.txt /
RUN pip install -r requirements.txt

WORKDIR /src
COPY src /src

RUN chown -R uwsgi:uwsgi /src

USER uwsgi
EXPOSE 9090

ARG UWSGI_EXTRA_ARGS

#CMD uwsgi --http :9090 --wsgi-file /src/uwsgi.py --callable app --enable-threads --http-timeout 120 --master -p 8 -b 65535 $UWSGI_EXTRA_ARGS
CMD uwsgi --ini /src/uwsgi.ini --http :9090  --callable app --enable-threads --http-timeout 120   -b 65535 $UWSGI_EXTRA_ARGS
