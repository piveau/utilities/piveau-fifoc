# piveau-fifoc

piveau file format converter

<p>piveau-fifoc uses these tools: <a href="https://pandoc.org/">pandoc</a>, <a href="https://pypi.org/project/pdfkit/">pdfkit</a>, <a href="https://pypi.org/project/PyPandas/">pypandas</a>, <a href="https://pypi.org/project/docx2pdf/">docx2pdf</a>, <a href="https://pypi.org/project/odfpy/">odfpy</a>, <a href="https://pypi.org/project/json2xml/">json2xml</a>, <a href="https://pypi.org/project/docx2txt/">docx2txt</a>, <a href="https://pypi.org/project/Csv2Xml/">csv2xml, <a href="https://pypi.org/project/pdf2docx//">pdf2docx</a>, <a href="https://pypi.org/project/xlsx2html/">xlsx2html</a>.

## Getting started
The overall goal is to develop features to be able to export data in multiple formats if the source data allows it.
 The service should offer the following features:

- API endpoint for
- receiving file to convert
- receiving a URL to a file to convert
- parameter with target format

## Input/output Formats
piveau-fifoc can convert from left to the right in following formats:
<table class="FormatsTable">
    <thead>
    <tr>
        <th>Source file Format</th>
        <th></th>
        <th>Target file format</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td>HTML</td>
        <th>===></th>
        <td>pdf, docx, json, odt, rtf</td>
    </tr>
    <tr>
        <td>CSV</td>
        <th>===></th>
        <td>docx, html, json, odt, rtf, xlsx, xml</td>
    </tr>
    <tr>
        <td>JSON</td>
        <th>===></th>
        <td>xml</td>
    </tr>
    <tr>
        <td>ODT</td>
        <th>===></th>
        <td>docx, html, json, rtf</td>
    </tr>
    <tr>
        <td>DOCX</td>
        <th>===></th>
        <td>pptx, odt, pdf, txt, html, json, odt, rtf</td>
    </tr>
    <tr>
        <td>XLSX, XLS</td>
        <th>===></th>
        <td>csv</td>
    </tr>
    <tr>
        <td>XLSX</td>
        <th>===></th>
        <td>html</td>
    </tr>
    <tr>
        <td>PDF</td>
        <th>===></th>
        <td>txt, docx</td>
    </tr>
    </tbody>
</table>


## Usage
using curl to convert file:
```bash
curl -X POST --data-binary @sample.csv http://localhost:5000/v1/convert/csv/json/ > username.json
curl -X POST --data-binary @sample.csv http://localhost:5000/v1/convert/csv/xlsx/ > output.xlsx
curl -X POST --data-binary @example.docx http://localhost:5000/v1/convert/docx/pdf/ > output.pdf

# using curl to convert from url:
curl http://localhost:5000/v1/convert/csv/xlsx/?url=https://somewhere.com/example.csv > output.xlsx
curl http://localhost:5000/v1/convert/csv/xlsx/?url=https://somewhere.com/example.csv > output.xlsx
```
## API

- '/' [Swagger UI], methods=\['GET'\]
- '/help' [Help], methods=\['GET'\]
- /v1/convert/&lt;in\_format&gt;/&lt;out\_format&gt;/, methods=\['POST'\]

input contents are in raw post data and output is in raw post response.

- /v1/convert/&lt;in\_format&gt;/&lt;out\_format&gt;/, methods=\['GET'\]

input named parameter is the URL to convert and output is in raw post
response.

- /v1/formats/&lt;in\_format&gt;/, methods=\['GET'\]

Returns the list of supported output formats for the given input format.
## Name
piveau-fifoc file format converter a service for converting files from one format in another

## Installation
There are two ways to start this application.

**1. Using Docker**

```bash
git clone <gitrepouri>
cd piveau-fifoc
docker build -t piveau-fifoc .
docker-compose up
```

## APP environments
**LOGGING_FILE_PATH** default is=os temp directory

**LOGGING_FILENAME** default is=fifoc-app.log

Maximum of file size that can be convert in bytes (is about 50MB)

**MAX_FILE_SIZE** default is=42428800

**UWSGI_EXTRA_ARGS** you can add extra args to uwsgi in docker-compose to run the uwsgi
This is default
```
CMD uwsgi --http :9090 --wsgi-file /src/uwsgi.py --callable app --enable-threads -b 65535 $UWSGI_EXTRA_ARGS
```
**2. As a Python Application**

To start the service as a Python application, a **Python 3.9 or higher** installation is required.
It is recommended to use *virtualenv* ([User Guide](https://virtualenv.pypa.io/en/latest/userguide/)).
You can download it via *PyPi* ([Installation Guide](https://virtualenv.pypa.io/en/latest/installation/)):

You can now use the script `run.sh` or `run.cmd` to start a local server.

## Running Tests
There are for Formats CSV,HTML,JSON,MS Office and PDF test methods in directory tests, Use python
methods to run the tests.

You can now use the script `run-tests.sh` or `run-tests.cmd` to run all formats tests.

## Support
https://data.europa.eu/en/feedback/form

## Roadmap
The service shall be failure proof. Redirections shall be supported, if a file URL was given. Any potential failures shall be part of the response, e.g.:
- conversion issues
- issues loading via URL provided files
- uploading big file to convert


## License

[Apache License, Version 2.0](LICENSE.md)

## Dependencies and their licenses
Create/update 3rd-party licenses
> pip install pip-licenses
> pip-licenses --with-notice-file --with-license-file --output-file 3rd-party-licenses.csv --format csv --no-license-path
***
This [3rd-party-licenses.csv](3rd-party-licenses.csv) contains a list of the dependencies and their licenses.

## SBOM Generation Tool for Python
> python -m pip install cyclonedx-bom
> cyclonedx-py requirements --outfile sbom.json --schema-version 1.6 --output-format JSON 
